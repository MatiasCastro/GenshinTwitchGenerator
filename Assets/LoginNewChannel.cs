using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace TwitchChatConnect.Client
{
    public class LoginNewChannel : MonoBehaviour
    {
        [SerializeField] TwitchChatClient twitchChatClient;
        [SerializeField] Text text;

        public void NewChannel()
        {
            print(text.text);
            twitchChatClient.NewChannel(text.text);
            
        }
    }
}
