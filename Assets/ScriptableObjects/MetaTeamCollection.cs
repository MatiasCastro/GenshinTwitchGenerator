using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/MetaTeamCollection", order = 1)]
public class MetaTeamCollection : ScriptableObject
{
    public MetaTeam[] metaTeams;
}
