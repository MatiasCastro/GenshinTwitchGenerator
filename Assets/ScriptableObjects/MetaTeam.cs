using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/MetaTeam", order = 1)]
public class MetaTeam : ScriptableObject
{
    public string Name;
    public List<CharacterSO> characters;
}
