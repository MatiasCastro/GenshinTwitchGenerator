using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/CharacterList", order = 1)]
public class CharacterList : ScriptableObject
{
    public CharacterSO[] allCharacters;
    public CharacterSO[] dpsCharacters;
    public CharacterSO[] healerCharacters;
    public CharacterSO[] subDpsCharacters;
    public CharacterSO[] suppCharacters;
}
