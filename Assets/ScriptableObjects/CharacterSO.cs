using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;



[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/SpawnManagerScriptableObject", order = 1)]
public class CharacterSO : ScriptableObject
{
    public string Name;
    public Sprite MiniImage;
    public Sprite BigImage;
    public int votes=0;
    
    public enum Classes{Dps,SubDps,Supp,Healer};
    public Classes myClass;
    

}
