using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
namespace Genshin
{
    public class CardManager : MonoBehaviour
    {
        [SerializeField] CharacterList characterList;
        public Card[] team;
        public Card[] metaTeam;
        public int count = 0;
        Card myCard;
        [SerializeField]TextMeshProUGUI textMeshPro; 
        public void AssignNewCharacter(CharacterSO newCharacter)
        {
            foreach (Card card in team)
            {

                if (card.myCharacter == null)
                {
                    myCard = card;
                    count++;
                    card.myCharacter = newCharacter;
                    card.refreshImage(count);
                    break;
                }
            }
        }
        public IEnumerator RefreshMetaTeam()
        {
            yield return new WaitForSeconds(0.1f);
            count = 0;
            foreach (Card card in metaTeam)
            {
                card.myCharacter =TeamGenerator.GetInstance().MyTeam.characters[count];
                card.refreshImage(count);
                count++;
            }
            textMeshPro.SetText(TeamGenerator.GetInstance().MyTeam.name);
            count = 0;
        }

    }
}