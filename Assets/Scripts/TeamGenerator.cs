using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Genshin
{
    public class TeamGenerator : Singleton<TeamGenerator>
    {
        public CharacterList characterList;
        public List<CharacterSO> mylist;
        public MetaTeam MyTeam;
        [SerializeField] CardManager cardManager;
        public List<CharacterSO> characterBanned;
        public MetaTeamCollection metaTeamCollection;
        bool isBreak=false;

        public CharacterSO RandomCharacter()
        {
            bool ready = false;
            CharacterSO character;

            while (ready == false)
            {
                character = characterList.allCharacters[Random.Range(0, characterList.allCharacters.Length - 1)];
                if (!characterBanned.Contains(character))
                {
                    print(character.Name);
                    ready = true;
                    if (mylist.Contains(character))
                        ready = false;
                    else
                    {
                        cardManager.AssignNewCharacter(character);
                        return character;
                    }
                }
            }
            return null;
        }
        public void ClearList()
        {
            foreach (CharacterSO toRemove in characterList.allCharacters)
                mylist.Remove(toRemove);
            for (int i = 0; i < cardManager.team.Length; i++)
            {
                cardManager.team[i].myCharacter = null;

            }
            cardManager.count = 0;


        }
        public void NewTeam()
        {
            ClearList();
            mylist.Add(RandomCharacter());
            mylist.Add(RandomCharacter());
            mylist.Add(RandomCharacter());
            mylist.Add(RandomCharacter());
            mylist.Add(RandomCharacter());
            mylist.Add(RandomCharacter());
            mylist.Add(RandomCharacter());
            mylist.Add(RandomCharacter());
        }
        public MetaTeam MetaTeam()
        {
            bool ready = false;
            MetaTeam team;
            
            while (ready == false)
            {
                isBreak=false;
                team = metaTeamCollection.metaTeams[Random.Range(0, metaTeamCollection.metaTeams.Length - 1)];
                foreach (CharacterSO banned in characterBanned)
                {
                    if (team.characters.Contains(banned))
                    {
                        isBreak=true;
                        break;
                    }
                }
                if (isBreak==false)
                    return team;
                    
            }
            return null;
        }
        public void NewTeamMeta()
        {
            ClearList();
            MyTeam =MetaTeam();
            StartCoroutine (cardManager.RefreshMetaTeam());
        }
    }
}
