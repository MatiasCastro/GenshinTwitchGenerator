using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Genshin
{
    public class BanManager : MonoBehaviour
    {
        [SerializeField] BanCharacter[] allCharacters;
        void Start()
        {
            for(int i=0;i<allCharacters.Length;i++)
            {
                allCharacters[i].myCharacter=TeamGenerator.GetInstance().characterList.allCharacters[i];
                allCharacters[i].myImage.sprite=allCharacters[i].myCharacter.MiniImage;
            }
                
        }

    }
}