using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomCardAnimation : MonoBehaviour
{
    [SerializeField] CharacterList characterList;
    [SerializeField] Image image;
    IEnumerator RandomCharacter()
    {
        for(int i=0;i<20;i++)
        {
            yield return new WaitForSeconds(0.1f);
            image.sprite=characterList.allCharacters[Random.Range(0,characterList.allCharacters.Length-1)].BigImage;
        }
        for(int i=0;i<10;i++)
        {
            yield return new WaitForSeconds(0.2f);
            image.sprite=characterList.allCharacters[Random.Range(0,characterList.allCharacters.Length-1)].BigImage;
        }
        for(int i=0;i<5;i++)
        {
            yield return new WaitForSeconds(0.5f);
            image.sprite=characterList.allCharacters[Random.Range(0,characterList.allCharacters.Length-1)].BigImage;
        }
        for(int i=0;i<4;i++)
        {
            yield return new WaitForSeconds(1f);
            image.sprite=characterList.allCharacters[Random.Range(0,characterList.allCharacters.Length-1)].BigImage;
        }
        for(int i=0;i<2;i++)
        {
            yield return new WaitForSeconds(2f);
            image.sprite=characterList.allCharacters[Random.Range(0,characterList.allCharacters.Length-1)].BigImage;
        }
        
    }
    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
        StartCoroutine(RandomCharacter());
    }
}
