using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelManager : MonoBehaviour
{
    [SerializeField] GameObject[] panels;

    public void SwitchPanel(int i)
    {
        panels[i].SetActive(true);
        for(int j=0;j<panels.Length;j++)
        {
            if(j!=i)
                panels[j].SetActive(false);
        }
    }
}
