using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Card : MonoBehaviour
{
    public CharacterSO myCharacter;
    [SerializeField] CharacterList characterList;
    public Image myimage;
    public PotaTween slectedPotatween;
    public PotaTween fadeIn;
    public IEnumerator RandomCharacter(int count)
    {
        for(int i=0;i<18*count;i++)
        {
            yield return new WaitForSeconds(0.1f);
            myimage.sprite=characterList.allCharacters[Random.Range(0,characterList.allCharacters.Length-1)].BigImage;
        }
        for(int i=0;i<5;i++)
        {
            yield return new WaitForSeconds(0.2f);
            myimage.sprite=characterList.allCharacters[Random.Range(0,characterList.allCharacters.Length-1)].BigImage;
        }
        for(int i=0;i<2;i++)
        {
            yield return new WaitForSeconds(0.5f);
            myimage.sprite=characterList.allCharacters[Random.Range(0,characterList.allCharacters.Length-1)].BigImage;
        }
        for(int i=0;i<1;i++)
        {
            yield return new WaitForSeconds(1f);
            myimage.sprite=characterList.allCharacters[Random.Range(0,characterList.allCharacters.Length-1)].BigImage;
        }
        
        myimage.sprite=myCharacter.BigImage;
    }
    public void refreshImage(int count)
    {
        StartCoroutine(RandomCharacter(count));
        
    }
}
