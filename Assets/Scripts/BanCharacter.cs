using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Genshin
{
    public class BanCharacter : MonoBehaviour
    {
        public CharacterSO myCharacter;
        public Image myImage;
        public void BanThisCharacter()
        {
            if (TeamGenerator.GetInstance().characterBanned.Contains(myCharacter))
                TeamGenerator.GetInstance().characterBanned.Remove(myCharacter);
            else
                TeamGenerator.GetInstance().characterBanned.Add(myCharacter);
        }
    }
}