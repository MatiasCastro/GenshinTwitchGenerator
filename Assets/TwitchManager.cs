using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TwitchChatConnect.Client;
using TwitchChatConnect.Data;
using TMPro;
using UnityEngine.UI;

namespace Genshin
{
    public class TwitchManager : MonoBehaviour
    {
        [SerializeField] int timeBetweenCharacterPick=10;
        [SerializeField] int charactersCount=0;
        [SerializeField] int timer2;
        [SerializeField]TextMeshProUGUI timer;
        public Card[] myTwitchTeam;
        // Start is called before the first frame update
        TeamGenerator myTeam => TeamGenerator.GetInstance();
        public CharacterSO GetWinner()
        {
            CharacterSO max=myTeam.RandomCharacter();
            foreach(CharacterSO winner in myTeam.characterList.allCharacters)
            {
                if (winner.votes>max.votes)
                    if (!myTeam.characterBanned.Contains(winner))
                        if(!myTeam.mylist.Contains(winner))
                            max=winner;
            }
            return(max);
        }
        private void clearVotes()
        {
            foreach(CharacterSO character in myTeam.characterList.allCharacters)
            {
                character.votes=0;
            }
        }
        public void Connect()
        {
            myTeam.ClearList();
            StartCoroutine(timmer());
            /*TwitchChatClient.instance.Init(onSuccess: () =>
            {
                TwitchChatClient.instance.onChatCommandReceived += OnChatCommandRecived;
                StartCoroutine(timmer());
                //TwitchChatClient.instance.onChatMessageReceived += OnFirstChatRecived;
            }, message =>
            {
                print("error al conectarse");
            });*/
        }

        IEnumerator timmer()
        {
            print("entro a el picker");
            yield return new WaitForSeconds(1f);
            clearVotes();
            myTwitchTeam[charactersCount].fadeIn.Play();
            myTwitchTeam[charactersCount].slectedPotatween.Play();
            timer.text=timeBetweenCharacterPick.ToString();
            for(int i=0; i<=timeBetweenCharacterPick;i++)
            {
                yield return new WaitForSeconds(1f);
                timer.text=(timeBetweenCharacterPick-i).ToString();
            }
            CharacterSO winner=GetWinner();
            myTeam.mylist.Add(winner);
            myTwitchTeam[charactersCount].myCharacter=winner;
            print("entro la ruleta");

            yield return StartCoroutine(myTwitchTeam[charactersCount].RandomCharacter(timer2));
            
            print("termino la ruleta");
            myTwitchTeam[charactersCount].slectedPotatween.Stop();
            charactersCount++;

            if(charactersCount<8)
            {
                StartCoroutine(timmer());
            }

        }

        // Update is called once per frame
        
        private void OnChatCommandRecived(TwitchChatCommand twitchChatCommand)
        {
            string command = twitchChatCommand.Message.Replace("!", string.Empty).ToLower();

            print(command);

            foreach (CharacterSO savedName in myTeam.characterList.allCharacters)
            {
                if (command == savedName.name.ToLower())
                {
                    savedName.votes++;
                    print(command + " +1 = "+ savedName.votes);
                    break;
                }
            }
        }

    }
}